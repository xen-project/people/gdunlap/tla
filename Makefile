TLATOOLS_JAR = tla2tools.jar

%.tla: %.pcal
	cp $< $@
	java -cp $(TLATOOLS_JAR) pcal.trans -nocfg $@
	rm -f $*.old

.PHONY: all-sources
all: all-sources

SOURCETARGETS += XSA287.tla
TESTTARGETS += XSA287-A.out
XSA287-A.out: XSA287-A.cfg XSA287.tla
	java -cp $(TLATOOLS_JAR) -XX:+UseParallelGC tlc2.TLC -config $< -workers 8 XSA287.tla | tee $@

SOURCETARGETS += XSA287fixed.tla
TESTTARGETS += XSA287fixed-A.out
XSA287fixed-A.out: XSA287-A.cfg XSA287fixed.tla
	java -cp $(TLATOOLS_JAR) -XX:+UseParallelGC tlc2.TLC -config $< -workers 8 XSA287fixed.tla | tee $@


SOURCETARGETS += XSA299.tla
TESTTARGETS += XSA299-s1p2.out
XSA299-s1p2.out: XSA299-s1p2.cfg XSA299.tla
	java -cp $(TLATOOLS_JAR) -XX:+UseParallelGC tlc2.TLC -config $< -workers 8 -difftrace XSA299.tla | tee $@

TESTTARGETS += XSA299-s2p3.out
XSA299-s2p3.out: XSA299-s2p3.cfg XSA299.tla
	java -cp $(TLATOOLS_JAR) -XX:+UseParallelGC tlc2.TLC -config $< -workers 8 -difftrace XSA299.tla | tee $@

SOURCETARGETS += XSA299nested.tla
TESTTARGETS += XSA299nested.out
XSA299nested.out: XSA299nested.cfg XSA299nested.tla
	java -cp $(TLATOOLS_JAR) -XX:+UseParallelGC tlc2.TLC -config $< -workers 8 -difftrace XSA299nested.tla | tee $@

.PHONY: show-targets
show-targets:
	@echo $(TESTTARGETS)

all-sources: $(SOURCETARGETS)

clean:
	rm -f *.tla *.out
