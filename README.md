# TLA+ specs for some key bits of Xen code

While working on [XSA-299], I reached a point where it was very
difficult to keep everything in my head.  Some of the attacks take
three processors and several steps to execute.  I had recently heard
about TLA+ on [Hacker News](https://news.ycombinator.com/) and so
decided to give it a spin.

I started with XSA-287, which had (what I believed to be) a correct
fix.  I implemented the original version to see if it found the race
condition (which it did), and then implemented the fixed version, in
which it found no issues.  Emboldened by this, I modeled my solution
to XSA-299.

**Disclaimer**: I'm far from a TLA expert; these are my first and only
TLA specs.

# Quick start

* Clone this repository

* Download the latest stable version of `tlaplus.jar` from the [TLA
Releases Page](https://github.com/tlaplus/tlaplus/releases/), and
place it in the git directory

* Run `make` to compile all the PlusCal files into TLA (will be quick)

* Run `make show-targets` to show what tests there are to run

Easiest demonstration would be to run `make XSA287-A.out`.  This is
the unfixed code; it will fairly quickly find a violation of the
constraints.

Then run ``make XSA287fixed-A.out`.  This runs with the same model
parameters, but a corrected version of the algorithm.  This takes 15
minutes to verify all possible states on my 8-thread processor.

# General approach

TLA is a very powerful but very quirky tool.  The most approachable
way to generate spec is to write tin PlusCal, a language sort of like
C.  This must still be translated to TLA in order for the checker to
run.

The default way to do this is to write PlusCal in a comment in TLA
(!), and then run a PlusCal compiler, which will modify a section
later on in the same file (!).  The result makes it annoying to check
into git, because you have a mix of hand-written and generated code in
the same file.

What this makefile does is to try to separate these steps out.
`.pcal` files are TLA files with untranslated PlusCal in them.  Then
there's a Makefile rule which 'compiles' `foo.pcal` into `foo.tla`.

Then every "model" has a target which will combine the approriate
`.tla` and `.cfg` files.

[XSA-299] https://xenbits.xen.org/xsa/advisory-299.html

